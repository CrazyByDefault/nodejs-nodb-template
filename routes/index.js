// write express middleware that says hello world
const router = require('express').Router();
// const queryAnyApi = require('../helpers/queryAnyApi');

const randomFunction = require('../controllers/index.js');

router.get('/', (req, res) => {
  res.send('Hello World!');
});

router.post('/tmp', (req, res) => {
  const { queryString, language } = req.body;

  // TODO: implement a query to the relevant API to fetch results
  
  const results = [
    {
      title: ``,
      html: '<p>This is the first example answer</p>',
    }
  ];
  res.send('Hello World!');
});

router.get('/get-query-url', randomFunction);

module.exports = router;