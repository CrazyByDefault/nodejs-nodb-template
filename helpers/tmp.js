// improt sleep
// import sleep from 'sleep';

async function test(number) {
  // return hello world
  setTimeout(() => {
  }, 1000);
  return `hello world ${number}`;
}

console.log(await test(1));
console.log(await test(2));
console.log(await test(3));

// test(1).then(
//   (result) => {
//     console.log(result);
//   }
// );

Promise.all([test(1), test(2), test(3)]).then(
  (result) => {
    console.log(result);
  }
);


// console.log(result);