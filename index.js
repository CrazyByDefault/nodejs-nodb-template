// Setup express server
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;

// import routes
const indexRoutes = require('./routes/index.js');
// const queryRoutes = require('./routes/query');

// setup middleware
// app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// start server
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

// use routes
app.use('/', indexRoutes);
// app.use('/query', queryRoutes);



