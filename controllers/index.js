function newController(req, res) {
  const name = req.query.name || 'bla';
  res.send(`hello world ${name}`);
}

module.exports = newController;
